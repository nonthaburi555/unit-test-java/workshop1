package kbtg.unittests.workshop01;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import kbtg.unittests.workshop01.business.TenDigitsISBNValidation;
import kbtg.unittests.workshop01.business.ThirteenDigitsISBNValidation;
import kbtg.unittests.workshop01.controller.ISBNValidatorController;
import kbtg.unittests.workshop01.model.ValidationEntity;


/**
 * Run with Mockito JUnit Test Runner ...
 * Please focus on how to create Several Test Cases to validate the ISBN Numbers
 * 
 * Mockito and JUnit is out of scope of this exercise
 * 
 * @author kovitad.j
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ISBNControllerTest {

	@InjectMocks
	ISBNValidatorController controller;
	
	/**
	 * We spy on Ten Digits Validator to inject this to our Controller
	 * 
	 */
	@Spy
	private TenDigitsISBNValidation tenDigitsIsbnValidator;

	/**
	 * We spy on Thirteen Digits Validator to inject this to our Controller
	 * 
	 */
	@Spy
	private ThirteenDigitsISBNValidation thirteenDigitsIsbnValidator;
	
	/**
	 * The Example of Positive Test Case
	 * 
	 * Validate 10 Digits ISBN Success
	 * 
	 */
	@Test
	public void testValidateISBNFail() {
		 //Setup valid  ISBN
		String isbnNumber = "1503242706";
		// Act
		ValidationEntity actual = controller.validateEightDigitsISBNNumber(isbnNumber);

		// Assert
		Assert.assertFalse(actual.isValid());
		
	}
	
	
}
