package kbtg.unittests.workshop01;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import kbtg.unittests.workshop01.business.ThirteenDigitsISBNValidation;


/**
 * 
 * 13 Digits Validation Test Class
 * 
 * @author kovitad.j
 *
 */
public class ThirteenDigitsISBNValidationTest {

	private ThirteenDigitsISBNValidation thirteenDigitsIsbnValidator;
	
	@Before
	public void setup() {
		thirteenDigitsIsbnValidator = new ThirteenDigitsISBNValidation();
	}
	/**
	 * This is the example of test scenario to help us the write the unit test 
	 * 
	 */
    @Test
	public void testValidateThirteenDigitsISBNFail() {
		// Setup valid 13 digits ISBN
		String isbnNumber = "978--0-7167-0344-1";
		// Act
		boolean isValid = thirteenDigitsIsbnValidator.validate(isbnNumber);
		// Assert
		Assert.assertFalse(isValid);
	}

}
