package kbtg.unittests.workshop01;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import kbtg.unittests.workshop01.business.TenDigitsISBNValidation;

/**
 * 10 Digits Validaton Test Class
 * 
 * @author kovitad.j
 *
 */

public class TenDigitsISBNValidationTest {
	
	private TenDigitsISBNValidation tenDigitsIsbnValidator;
	
	@Before
	public void setup() {
		tenDigitsIsbnValidator = new TenDigitsISBNValidation();
	}
	
	/**
	 * This is the example of test scenario to help us the write the unit test 
	 * 
	 */
	@Test
	public void testValidateTenDigitsISBNFail() {
		// Setup valid 10 digits ISBN
		String isbnNumber = "0--7167-0344-1";
		// Act
		boolean isValid = tenDigitsIsbnValidator.validate(isbnNumber);
		// Assert
		Assert.assertFalse(isValid);
	}

	

}
