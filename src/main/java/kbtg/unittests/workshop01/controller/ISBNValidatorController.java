package kbtg.unittests.workshop01.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import kbtg.unittests.workshop01.business.TenDigitsISBNValidation;
import kbtg.unittests.workshop01.business.ThirteenDigitsISBNValidation;
import kbtg.unittests.workshop01.model.ValidationEntity;

/**
 * Rest Controller to export the validation path
 * 
 * @author kovitad.j
 *
 */

@RestController
public class ISBNValidatorController {

	@Autowired
	private TenDigitsISBNValidation tenDigitsIsbnValidator;

	@Autowired
	private ThirteenDigitsISBNValidation thirteenDigitsIsbnValidator;

	@GetMapping(path = "/book/validate/tendigits/{isbn}")
	public ValidationEntity validateEightDigitsISBNNumber(@PathVariable("isbn") String isbnNumber) {
		boolean isValid = tenDigitsIsbnValidator.validate(isbnNumber);
		ValidationEntity result = new ValidationEntity();
		if (isValid) {
			result.setMessage("Valid Ten Digits ISBN Number");
		} else {
			result.setMessage("Invalid Ten Digits ISBN Number");
		}
		result.setValid(isValid);

		return result;
	}

	@GetMapping(path = "/book/validate/thirteendigits/{isbn}")
	public ValidationEntity validateThirTheenDigitsISBNNumber(@PathVariable("isbn") String isbnNumber) {
		boolean isValid = thirteenDigitsIsbnValidator.validate(isbnNumber);
		ValidationEntity result = new ValidationEntity();
		if (isValid) {
			result.setMessage("Valid Thirteen Digits ISBN Number");
		} else {
			result.setMessage("Invalid Thirteen Digits ISBN Number");
		}
		result.setValid(isValid);
		return result;
	}
}
