package kbtg.unittests.workshop01.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ApplicationUtils {
	
	
	public static Matcher createPattern(final String isbnNumber, final String validationPattern) {
		Pattern pattern = Pattern.compile(validationPattern);
		Matcher matcher = pattern.matcher("");
		matcher.reset(isbnNumber);
		return matcher;
	}
}
