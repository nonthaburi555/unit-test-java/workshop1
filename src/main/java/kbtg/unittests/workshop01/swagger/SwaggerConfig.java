package kbtg.unittests.workshop01.swagger;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Bean
	public Docket selectApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("kbtg.unittests.workshop01.controller"))
				.build().apiInfo(apiInfo());
	}

	private ApiInfo apiInfo() {
		return new ApiInfo("Work Shop 01 REST API", 
				"ISBN Validation APIs.", "APIs", "Terms of service",
				new Contact("Kovitad Janlakhon", "www.kbtg.tech", "kovitad.j@kbtg.tech"), "License of API",
				"API license URL", Collections.emptyList());
	}
}
