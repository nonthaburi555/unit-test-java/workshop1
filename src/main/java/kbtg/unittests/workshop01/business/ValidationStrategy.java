package kbtg.unittests.workshop01.business;

/**
 * The Validation Strategy Interface to Validate ISBN Number
 * 
 * @author kovitad.j
 *
 */

public interface ValidationStrategy {
	
	public boolean validate(final String isbnNumber);
}
