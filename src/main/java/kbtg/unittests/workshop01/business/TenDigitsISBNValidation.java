package kbtg.unittests.workshop01.business;

import java.util.regex.Matcher;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import kbtg.unittests.workshop01.utils.ApplicationUtils;

@Service
public class TenDigitsISBNValidation implements ValidationStrategy {

	//0-7-167-0344-0
	public static final String TEN_DIGITS_ISBN_FORMATTER = "^[0-9]{1}[-][0-9]{4}[-][0-9]{4}[-][0-9]{1}$";

	public boolean validate(final String isbnNumber) {

		if (StringUtils.isEmpty(isbnNumber)) {
			throw new IllegalArgumentException();
		}
		final Matcher matcher = ApplicationUtils.createPattern(isbnNumber, TEN_DIGITS_ISBN_FORMATTER);
		if (matcher.matches()) {
			final String numbericOnlyIsbn = isbnNumber.replaceAll("[^\\d]", "");
			int total = 0;
			for (int i = 0; i < 10; i++) {
				total += numbericOnlyIsbn.charAt(i) * (10 - i);
			}
			if (total % 11 == 0) {
				return true;
			}
		}
		return false;
	}


}
