package kbtg.unittests.workshop01.business;

import java.util.regex.Matcher;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import kbtg.unittests.workshop01.utils.ApplicationUtils;

@Service
public class ThirteenDigitsISBNValidation implements ValidationStrategy {

	public static final String THIRTEEN_DIGITS_ISBN_FORMAT = "^[0-9]{3}[-][0-9]{1}[-][0-9]{4}[-][0-9]{4}[-][0-9]{1}$";

	public boolean validate(final String isbnNumber) {
		if (StringUtils.isEmpty(isbnNumber)) {
			throw new IllegalArgumentException();
		}
		final Matcher matcher = ApplicationUtils.createPattern(isbnNumber, THIRTEEN_DIGITS_ISBN_FORMAT);
		if (matcher.matches()) {
			final String numbericOnlyIsbn = isbnNumber.replaceAll("[^\\d]", "");
			int position = 13;
			int total = 0;
			for (int i = 0; i < 13; i++) {
				total += numbericOnlyIsbn.charAt(i) * (position % 2 != 0 ? 1 : 3);
				position--;
			}
			if (total % 10 == 0) {
				return true;
			}
		}
		return false;

	}

}
